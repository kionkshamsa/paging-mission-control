from datetime import datetime, timedelta
import pprint

def construct_satelite_data(input_str: str) -> dict:

    satelite_data = {}

    # replace the space between ID and timestamp with a pipe so that everything splits correctly
    split_alert_message = input_str.split("|")
    date_time_format_string = "%Y%m%d %H:%M:%S.%f"

    satelite_data["timestamp"]         = datetime.strptime(split_alert_message[0], date_time_format_string)
    satelite_data["satelliteId"]       = int(split_alert_message[1])
    satelite_data["red-high-limit"]    = int(split_alert_message[2])
    satelite_data["yellow-high-limit"] = int(split_alert_message[3])
    satelite_data["yellow-low-limit"]  = int(split_alert_message[4])
    satelite_data["red-low-limit"]     = int(split_alert_message[5])
    satelite_data["raw-value"]         = float(split_alert_message[6])
    satelite_data["component"]         = split_alert_message[7].strip()
    satelite_data["severity"]          = ""

    return satelite_data

def get_satelite_data() -> list:

    """
        Get the data from input.txt, read it line by line, and split it on |
    """

    data = []

    input_file  = open("input.txt", "r") # open the contents of the file
    input_lines = input_file.readlines() # split the file line by line

    for input in input_lines:
        satelite_data = construct_satelite_data(input)
        data.append(satelite_data)
    
    return data


def set_intervals(logs: list) -> dict:

    """
        Create a dictionary split across 5 minute intervals, 
        inside each interval store each log based off of 
        satelite ID

        this code assumes we are only testing on the test 
        data listed on the example, if this ran on mulitple
        hours this code would have false positives
    """

    logs_by_intervals = {}

    for interval in range(5, 60, 5):
        logs_by_intervals[str(interval)] = {}

        for log in logs:
            
            if not str(log["satelliteId"]) in logs_by_intervals[str(interval)]:
                logs_by_intervals[str(interval)][str(log["satelliteId"])] = []

            if log["timestamp"].minute < interval and log["timestamp"].minute >= interval - 5:
                logs_by_intervals[str(interval)][str(log["satelliteId"])].append(log)

    return logs_by_intervals



def check_components(logs: list) -> None:

    """
        looping over logs under each satelite ID and checking battery and the thermostat
    """

    bat_error_count   = 0
    therm_error_count = 0

    bat_print   = False
    therm_print = False

    for log in logs:
        if log["component"] == "BATT" and log["raw-value"] < log["red-low-limit"]:
            bat_error_count += 1
        
        elif log["component"] == "TSTAT" and log["raw-value"] > log["red-high-limit"]:
            therm_error_count += 1

        # The hiring manager wanted me to fix how my output was formatted and
        # re submit the project so instead of just doing print(log) or pprint(log) 
        # or json.dumps(log) I'm just gonna be very specific with my printing...
        if therm_error_count >= 3 and not therm_print:
            log["severity"] = "RED HIGH"
            therm_print = True
            print("[")
            print("\t{")
            print('\t\t"satelliteId":{0},'.format(log["satelliteId"]))
            print('\t\t"severity":"{0}",'.format(log["severity"]))
            print('\t\t"component":"{0}",'.format(log["component"]))
            print('\t\t"timestamp":"{0}"'.format(str(log["timestamp"])))
            print("\t},")

        if bat_error_count >= 3 and not bat_print:
            log["severity"] = "RED LOW"
            bat_print = True
            print("[")
            print("\t{")
            print('\t\t"satelliteId":{0},'.format(log["satelliteId"]))
            print('\t\t"severity":"{0}",'.format(log["severity"]))
            print('\t\t"component":"{0}",'.format(log["component"]))
            print('\t\t"timestamp":"{0}"'.format(str(log["timestamp"])))
            print("\t}")
            print("]")
        


def check_satelite_data() -> list:
    logs = get_satelite_data()
    logs_by_intervals = set_intervals(logs)

    for time_interval in logs_by_intervals:
        for satelite_id in logs_by_intervals[time_interval]:
            check_components(logs_by_intervals[time_interval][satelite_id]) 

            

if __name__ == "__main__":
    check_satelite_data()